Once everything was up and running from a single file, running with

```go run main.go```

I made the application a module.  This allows it to build into a binary with all the dependencies in it.

To set this up

```go mod init books-list```

This created the go.mod and go.sum files which contain the dependency versions.

It also created books-list which is the binary which can be run

```./books-list```
