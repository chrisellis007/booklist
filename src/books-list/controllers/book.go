package controllers

import (
	"books-list/models"
	bookRepository "books-list/repository/book"
	"books-list/utils"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Controller not sure why this is like this...
type Controller struct{}

var books []models.Book

// GetBooks takes a db instance from the Router
func (c Controller) GetBooks(db *sql.DB) http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		var error models.Error
		books = []models.Book{}
		// this seems to create a reference to the file
		bookRepo := bookRepository.BookRepository{}
		books, err := bookRepo.GetBooks(db, book, books)

		if err != nil {
			error.Message = "Error fetching book list"
			// send generic error message
			utils.SendError(w, http.StatusInternalServerError, error)
			return
		}

		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, books)
	}
}

// GetBook takes a db instance from the Router
func (c Controller) GetBook(db *sql.DB) http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		// this is a mux thing that gets a map of the parameters
		params := mux.Vars(r)
		id, _ := strconv.Atoi(params["id"])
		bookRepo := bookRepository.BookRepository{}
		book, err := bookRepo.GetBook(db, id, book)

		if err != nil {
			var dbError models.Error
			if err == sql.ErrNoRows {
				dbError.Message = fmt.Sprintf("The id %d was not found", id)
				utils.SendError(w, http.StatusNotFound, dbError)
			} else {
				dbError.Message = "Database error"
				utils.SendError(w, http.StatusInternalServerError, dbError)
			}
			return
		}

		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, book)
	}
}

// AddBook takes a db instance from the Router
func (c Controller) AddBook(db *sql.DB) http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		var bookID int
		var dbError models.Error
		// this maps the value of the body into the book variable
		json.NewDecoder(r.Body).Decode(&book)
		if book.Year == "" || book.Author == "" || book.Title == "" {
			dbError.Message = "There is some fields missing"
			utils.SendError(w, http.StatusBadRequest, dbError)
			return
		}
		bookRepo := bookRepository.BookRepository{}
		bookID, err := bookRepo.AddBook(db, book)
		if err != nil {
			dbError.Message = "Error adding book"
			utils.SendError(w, http.StatusInternalServerError, dbError)
			return
		}
		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, bookID)
	}
}

// DeleteBook takes a db instance from the Router
func (c Controller) DeleteBook(db *sql.DB) http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		var rowsDeleted int64
		var removeError models.Error
		// this is a mux thing that gets a map of the parameters
		params := mux.Vars(r)
		id, _ := strconv.Atoi(params["id"])
		bookRepo := bookRepository.BookRepository{}
		rowsDeleted, err := bookRepo.DeleteBook(db, id)
		if err != nil || rowsDeleted == 0 {
			removeError.Message = fmt.Sprintf("There was an issue removing the book with id %d", id)
			utils.SendError(w, http.StatusInternalServerError, removeError)
			return

		}
		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, rowsDeleted)
	}
}

// UpdateBook takes a db instance from the Router
func (c Controller) UpdateBook(db *sql.DB) http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book
		var rowsAffected int64
		var updateError models.Error
		// get the book from the body (sent as a json object under raw data)
		// the decoded pointer gets sent to the book variable declared above
		json.NewDecoder(r.Body).Decode(&book)
		// reflect is a package for looking at types
		// log.Println(reflect.TypeOf(params["id"]))
		if book.ID == 0 || book.Year == "" || book.Author == "" || book.Title == "" {
			updateError.Message = "All fields are required"
			utils.SendError(w, http.StatusBadRequest, updateError)
			return
		}
		// log.Println("book = ", book)
		bookRepo := bookRepository.BookRepository{}
		rowsAffected, err := bookRepo.UpdateBook(db, book)
		if err != nil {
			updateError.Message = "Issue with database"
			utils.SendError(w, http.StatusInternalServerError, updateError)
			return
		}

		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, rowsAffected)
	}
}

// CheckHealth returns a success
func (c Controller) CheckHealth() http.HandlerFunc {
	// this is an anonymous version of the old function
	// not sure how the w and r variables are passed in
	return func(w http.ResponseWriter, r *http.Request) {
		// send generic success message, setting header first
		w.Header().Set("Content-type", "application/json")
		utils.SendSuccess(w, nil)
	}
}
