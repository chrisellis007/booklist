package bookRepository

import (
	"books-list/models"
	"database/sql"
)

// BookRepository doesn't seem to need anything in it
type BookRepository struct{}

// GetBooks returns a list of books
func (b BookRepository) GetBooks(db *sql.DB, book models.Book, books []models.Book) ([]models.Book, error) {
	rows, err := db.Query("select * from books")
	if err != nil {
		return []models.Book{}, err
	}

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
		books = append(books, book)
	}
	if err != nil {
		return []models.Book{}, err
	}
	return books, nil
}

// GetBook returns a list of books
func (b BookRepository) GetBook(db *sql.DB, id int, book models.Book) (models.Book, error) {
	row := db.QueryRow("select * from books where id=$1", id)
	err := row.Scan(&book.ID, &book.Title, &book.Author, &book.Year)
	return book, err
}

// AddBook returns the ID of the book added
func (b BookRepository) AddBook(db *sql.DB, book models.Book) (int, error) {
	err := db.QueryRow("insert into books (title, author, year) values($1,$2,$3) RETURNING id;",
		book.Title, book.Author, book.Year).Scan(&book.ID)
	if err != nil {
		return 0, err
	}
	return book.ID, err
}

// DeleteBook returns the number of row affected
func (b BookRepository) DeleteBook(db *sql.DB, id int) (int64, error) {

	result, err := db.Exec("delete from books where id = $1", id)
	if err != nil {
		return 0, err
	}
	rowsDeleted, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsDeleted, nil
}

// UpdateBook returns the number of row affected
func (b BookRepository) UpdateBook(db *sql.DB, book models.Book) (int64, error) {
	result, err := db.Exec("update books set title=$1, author=$2, year=$3 where id=$4 RETURNING id;",
		book.Title, book.Author, book.Year, book.ID)
	if err != nil {
		return 0, err
	}
	rowsUpdated, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}
	return rowsUpdated, nil
}
