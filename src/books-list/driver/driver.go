package driver

import (
	"database/sql"
	"log"
	"os"

	"github.com/lib/pq"
)

var db *sql.DB

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// ConnectDB is
func ConnectDB() *sql.DB {
	pgURL, err := pq.ParseURL(os.Getenv("POSTGRES_URL"))
	logFatal(err)

	// log.Println(pgURL)
	db, err = sql.Open("postgres", pgURL)
	logFatal(err)

	err = db.Ping()
	logFatal(err)
	log.Println("Connected to DB")

	return db
}
